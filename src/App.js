import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,Switch, Route, Redirect
} from 'react-router-dom';
import Translator from './components/translator';
import Profile from './components/profile';
import NotFound from './components/NotFound';
import Landing from './components/landing';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <React.Fragment>
      <Router>
        <div className="container">
          <Switch>
            <Route exact path="/translator" component={Translator}/>
            <Route  path="/landing" component={Landing}/>
            <Route  path="/profile" component={Profile}/>
            <Route exact path="/">
                <Redirect to="/landing"/></Route>
            <Route path="*" component={NotFound}/>
            </Switch>
        </div>
      </Router>
    </React.Fragment>
  );
}

export default App;
