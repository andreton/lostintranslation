import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route, Redirect,Link
    } from 'react-router-dom';
    import logo from '../Pictures/Logo.png'
class Landing extends Component {
    state = {
        name:'',
        showLink:false

      }
      //Changing state based on input in text field, hiding button is input is empty
      handleChange=(e)=>{
        this.setState({name:e.target.value});
        if(e.target.value===""){
            this.setState({showLink:false});
        }
        else{
            this.setState({showLink:true})
        }
        
      }
      //Setting the input name to localstorage
      setNameToLocalStorage=()=>{
            try{
                localStorage.setItem(this.state.name,'') 
            }
            catch(e){
                console.log("Something went wrong when writing to localstorage"+e);
            }
          }
      
    render() { 
        return (
            <div>
                <nav className="navbar navbar-light bg-warning">
             <span className="navbar-brand mb-0 h1" style={{fontFamily:'Love Ya Like A Sister'
                }}>Lost in translation</span>
             <div className="loggedInName">
                 <h2 style={{fontSize:"2em",fontFamily:'Love Ya Like A Sister'}}>{this.state.name} </h2>
             </div>
             </nav> 
             <div className="lost-in-translation-get-started bg-warning" style={{height:"200px", borderTop:"3px solid black", padding:"1em", textAlign:"center",justifyContent:"center" }}>
             <img src={logo} alt="Welcome" width="100px" height="auto" style={{float:"left", position:"static", left:"0px"}}></img>
                <div className="welcome-text" style={{fontFamily:'Love Ya Like A Sister',textAlign:"center"}} >
                    <h1 style={{textAlign:"center",margin:"0 auto",justifyContent:"center",marginRight:"100px"}}>Lost in translation</h1>
                        <h2 style={{marginRight:"100px"}}>Get started</h2></div> 
             </div>
             <div className="user-input-container" style={{textAlign:"center"}} >
                 <p><input type="text" placeholder='Your name' name='name'  style={{borderRadius:"10%"}} onChange={this.handleChange} required/></p>
                 {this.state.showLink===true ?
                 <Link className="bg-danger" to={{pathname:`/translator`,nameObject:this.state.name}}><span onClick={this.setNameToLocalStorage} style={{color:"white",padding:"1em 1em 1em 1em"}}>Register</span> </Link>:""}
                              </div>
          
             </div>
        );
    };
 
}

 
export default Landing;