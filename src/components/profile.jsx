import React, { Component,useState } from 'react'
import logo from '../Pictures/Logo.png'
import '../styling/profile.css';
import {Link
    } from 'react-router-dom';

class Profile extends Component {
    constructor(props){
        super(props)
        this.state = {
            name:this.props.location.nameObject,
            savedTranslations:[],
            arrayToDisplay:[]
          }
    }
    getItemsFromLocalStorage=()=>{
        const arrayFromLocalStorage=localStorage.getItem(this.state.name);
        try{
            this.setState({savedTranslations: JSON.parse(arrayFromLocalStorage)});

        }
        catch(e){
            console.log("Something went wrong with getting value from localstorage: "+e)
        }
    }
    componentDidMount=()=>{
        if(this.state.name==null){
            this.setState({name:localStorage.key(localStorage.length-1)})
        }
        else{
            return 
        }
       }

       displayTranslation=(inputTranslation)=>{
           const inputTranslationArray=inputTranslation.toLowerCase().replace(/ /g,"_").split('');
            this.setState({arrayToDisplay:inputTranslationArray})
    }
    renderFunction(){
        return ( <span style={{ display: "inline"}} >
        {this.state.arrayToDisplay.map((list,index)=>
            <img key={index} src={require('../Pictures/'+list+'.png') } width="70"></img>
       )}
        </span>)
    }
    resetLocalStorage=()=>{
        localStorage.removeItem(this.state.name);
    }
    render() {
        return (
             <div className="main-container bg-warning">
                <nav className="navbar navbar-light bg-warning">
                <img src={logo} alt="Welcome" width="50px" height="auto" style={{float:"left"}}></img>
             <span className="navbar-brand mb-0 h1" style={{fontFamily:'Love Ya Like A Sister'
                }}>Lost in translation</span>
             <div className="loggedInName">
                 <h2 style={{fontSize:"2em",fontFamily:'Love Ya Like A Sister'}}>{this.state.name} </h2>
             </div>
             </nav> 
             <div className="show-translations-or-start-over">
                <button className="view-translations" onClick={this.getItemsFromLocalStorage}>View translations</button>
                <Link id="link-to-profile" to={{pathname:`/landing`}}><span onClick={this.resetLocalStorage}>Start Over</span> </Link>
                </div>
                <br/>
                <div className="list-of-translations">
                <ul>
                {this.state.savedTranslations.map((savedTranslations,index)=>
                    <button id="translation-buttons" key={index} value={savedTranslations} onClick={e=>this.displayTranslation(e.target.value)} >{savedTranslations}    </button>)}
                </ul>
                </div>
                <div className="container-of-translations">
               <h3> {this.state.arrayToDisplay}</h3><br/>
                {this.renderFunction()}
                </div>
                <div></div>
            </div>
        )
    }
}
export default Profile;
