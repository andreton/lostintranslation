import React, { Component,useState } from 'react';
import { BrowserRouter as Router,
    Switch,
    Route, Redirect,Link
    } from 'react-router-dom';
    import logo from '../Pictures/Logo.png'
    import "../styling/translator.css"
class Translator extends Component {
    constructor(props){
        super(props)
        this.state = {
            name:this.props.location.nameObject,
            inputWord:'',
            outputWord:'',
            outputCharArray:[],
            arrayOfTranslatedWords:[]
          }
    }
    handleInputTextChange=(event)=>{
        if(!/^[a-zA-Z\s]+$/.test(event.target.value.charAt(event.target.value.length-1))){
            alert("Illegal input, please only use characters"); 
        }
        else{
            const inputVariable=event.target.value;
            this.setState({outputWord:inputVariable});
            this.setState({outputCharArray:event.target.value.toLowerCase().replace(/ /g,"_").split('')})
        } 
        }
    renderFunction(){
        return ( <span style={{ display: "inline"}} >
        {this.state.outputCharArray.map((list,index)=>
            <img key={index} src={require('../Pictures/'+list+'.png') } width="70"></img>
       )}
        </span>)
    }
    saveTranslation=()=>{
        try{
            this.setState({
                arrayOfTranslatedWords:
                [...this.state.arrayOfTranslatedWords,this.state.outputWord]
            },()=>{
                if(this.state.arrayOfTranslatedWords.length<4){
                    console.log(this.state.arrayOfTranslatedWords)
                    localStorage.setItem(this.state.name, JSON.stringify(this.state.arrayOfTranslatedWords)) 
                }
                else{
                    let arrayBoundry=[this.state.outputWord,...this.state.arrayOfTranslatedWords];
                    arrayBoundry=arrayBoundry.slice(0,4);
                    this.setState({arrayOfTranslatedWords:arrayBoundry})
                    localStorage.setItem(this.state.name, JSON.stringify(this.state.arrayOfTranslatedWords))  
                }
            })    
        }
        catch(e){
            console.log(e)
        }
        finally{
        }
       }
       

       componentDidMount() {
        if(this.state.name==null){
            this.setState({name:localStorage.key(localStorage.length-1)})
        }
        else{
            return 
        }
       }
    render() {
        return ( 
            <div className="main-container">
                <nav className="navbar navbar-light bg-warning">
                <img src={logo} alt="Welcome" width="50px" height="auto" style={{float:"left"}}></img>
             <span className="navbar-brand mb-0 h1" style={{fontFamily:'Love Ya Like A Sister'
                }}>Lost in translation</span>
             <div className="loggedInName">
                 <h2 style={{fontSize:"2em",fontFamily:'Love Ya Like A Sister'}}>{this.state.name} </h2>
             </div>
             </nav> 
             <div className="container-for-translation bg-warning">
            <label htmlFor="wordToTranslate" id="word-to-translate">Word to translate</label>
            <br/>
                <input type="text" id="input-word" name="input-word" onKeyUp={this.handleInputTextChange} ></input> 
                <br/>
                <label id="translated-word-header" htmlFor="translatedWord">Translated word</label>
                <br/>
                <div className="translated-word-output">
                {this.renderFunction()}
                </div>
                <h1>{this.state.outputWord}</h1>
                <div className="navigation-buttons"> 
                <button id="save-translation-button" onClick={this.saveTranslation}>Save translation</button><br/>
                <Link id="link-to-profile" to={{pathname:`/profile`,nameObject:this.state.name}}><span onClick={this.setNameToLocalStorage}>Profile page</span> </Link>   </div>
  
                </div>
            </div> );
    }
}
 
export default Translator;